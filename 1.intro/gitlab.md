# GitLab
Como já vimos na introdução sobre o [Git][git], os repositórios remotos dos nossos projetos ficam hospedados no Gitlab. Veremos agora em mais detalhes as manipulações que fazemos por lá durante nosso [fluxo de integração de código][workflow].

Primeiramente, certifique-se que sua [VPN está configurada][vpn] e que você tem acesso ao [GitLab][gitlab] e então podemos prosseguir.

[git]: /1.intro/git.md
[workflow]: /1.intro/workflow.md
[vpn]: /4.extra/vpn.md
[gitlab]: https://gitlab.nelogica.com.br

## Autenticação
Para acessar o [GitLab][gitlab], utilize seu email da Nelogica junto com a senha que te fora enviada durante. Essa deve ser a mesma do seu computador, mas caso tenha dificuldades, comunique seu facilitador que ele lhe ajudará a entrar em contato com um responsável da infra.

## Configuração da chave SSH
Após gerar um par de chaves [SSH][ssh] para seu computador, copie o conteúdo do arquivo `id_rsa.pub` presente no diretório `.ssh` dentro da pasta do seu usuário no Windows. Iremos registrá-la em sua conta do Gitlab, permitindo sua autenticação automática enquanto estivermos operando com o git.

Acesse [suas chaves SSH no Gitlab][gitlab-ssh], cole o conteúdo copiado no campo *chave*, defina uma data de expiração bem longa e clique em **adicionar chave**.
Pronto, sua chave foi adicionada com sucesso. E para conferir que está tudo certo você pode prosseguir com clone do repositório do welcome

[ssh]: /4.extra/ssh.md
[gitlab-ssh]: http://gitlab.nelogica.com.br/profile/keys


## Manipulando repositórios remotos
Além das interações executadas diretamente pela CLI do git, podemos realizar algumas ações sobre nossos repositórios remotos através da interface gráfica do Gitlab

### Fork
Um fork é a primeira etapa do [fluxo de integração de código][workflow] e serve para clonarmos remotamente um projeto e dessa maneira termos nossa própria e exclusiva versão do repositório que queremos evoluir. Essa opção estará disponível no canto esquerdo superior na página principal de qualquer repositório que você acessar pelo Gitlab.

Após realizar o fork do repositório desejado você encontrará sua cópia na sua lista de *projetos pessoais*, na sua página principal do Gitlab. Acessando esse repositório você terá acesso a sua *url ssh* através da ação *clone* no canto direito superior. Essa informação será usada na sequencia para realizar o clone localmente e prosseguir com a execução das suas modificações sobre aquele projeto.

### Merge Request
O merge request é uma das últimas etapas do [fluxo de integração de código][workflow] e é um mecanismo usado para que um desenvolvedor notifique os membros da equipe de que ele concluíu uma funcionalidade. Ele representa uma solicitação de integração de código ao projeto.

Enquanto aberto um merge request é usado como espaço de discussão e revisão do trabalho realizado, onde os integrantes da equipe podem registrar comentários sobre as modificações, dentre outras ações que favorecem a colaboração dessa tarefa.

Veremos como ele funciona na prática.

#### Criação do Merge Request
Existem 2 formas de realizar um Merge Request, a primeira é via linha de comando.
Desta forma após a realização de um Push via linha de comando algumas mensagens são apresentadas no seu terminal.

![Bash](/img/MR1.png?raw=true  "Merge Request Link")

Como é marcado na Figura acima, existe um link para realizar o Merge Request deste branch. Esse Link nos encaminha direto para uma nova abertura de um Merge Request, porém essa mesma nova abertura pode ser aberta de outra forma, está é a segunda forma de se abrir um Merge Request:
![WebMR](/img/MRWEB.png?raw=true  "Merge Request via Web")

Após a abertura do merge request é necessária a inclusão de algumas informações:
![WebMR_DESC](/img/WEBMR_DESC.png?raw=true  "Descrição do Merge Request")

 1. Titulo do Merge Request: Em sua maioria é a descrição básica já realizada no commit, mas vale a pena lembrar em seguir o padrão Nelogica.
 2. Descrição: Alguns times utilizam Templates para essa descrição, mas o ideia é descrever o motivo e a solução com imagens.
 3. Solicitante: Quem está solicitando o Merge Request, neste caso você.
 4. Revisor: Este será seu primeiro revisor do Merge Request, que vai analisar se está tudo certo no seu Merge.

 Depois da inclusão destas etapas era de esperar comentários e revisões feitas pelos seus revisores, nesse momento pode ter uma troca de mensagens através do Merge Request para solução de possíveis problemas ou dúvidas. Esse Merge Request pode sempre ser atualizado por você através de um novo commit na sua branch do Merge.
![MERGE_APPROVE](/img/MR_APPROVE.png?raw=true  "Esperando Aprovação Merge Request")

Um dos problemas que podem ocorrer durante um Merge Request é a versão da sua Branch estar desatualizada, dessa forma no Merge Request é avisado que é necessária a atualização da Branch.

![MR_B](/img/MR_B.png?raw=true  "Merge Request Behind")

No exemplo acima o branch está 28 atualizações atrasadas dessa forma é necessário que seja feito uma atualização no seu branch. Os seguintes comandos devem ser executados para que o branch seja atualizado.

    1 git checkout BRANCH
    2 git fetch upstream
    3 git rebase upstream/develop
    4 git push -f origin BRANCH
Neste caso o comando da linha 1 coloca no branch do merge request, o comando da linha 2 busca as atualizações que estão no upstream, caso não tenha o upstream apontado é necessário apontar como remote, como visto anteriormente. A linha 3 faz um rebase trazendo as atualizações do upstrem para o branch, neste caso o exemplo está como develop, mas ele deve estar alinhado de acordo com o projeto do git, podendo ser master,develop,... A linha 4 realiza o push forçado da sua atualização.

Outro problema que pode ocorrer é a quebra do pipeline, caso isso ocorra é necessário ver qual job do pipeline quebrou e ver o motivo e tentar corrigir o problema, e realizar um novo push para que a correção passe novamente pelo pipeline.

![pipeline](/img/pipeline1.png?raw=true  "Pipeline Fail")

Um item a ser lembrado é que cada etapa de acordo com cada projeto tem um pipeline diferente, ou seja são diferentes pipelines que precisam ser aprovados.


Para uma documentação direta do gitlab sobre Merge Request acesse a [documentação](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html) do gitlab.

## Padrão de commit
Na Nelogica, adotamos alguns padrões para a mensagem dos commits, o que simplifica a gerência e automatização do processo de integração de código, assim como permite a extração de estatísticas.

![Padrão de Commit](/img/CommitStandard.png?raw=true  "Padrão de commit")

Este padrão está fortemente atrelado ao *Jira* (que será discutido depois). O objetivo da mensagem é possuir uma descrição sucinta do que foi feito no commit. O objetivo da issue tag é atrelar o commit a um requisito de alto nível (descrito na hierarquia de *issues* do *Jira*). A tag de tipo do commit ajuda no parsing do repositório por informações e também força o desenvolvedor a manter os commits coesos (com somente uma modificação e um objetivo).


## Questões
- O que é o Gitlab?
- O que é uma chave pública/privada e qual sua utilidade?
- O que é um fork?
