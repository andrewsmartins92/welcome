# Fluxo de integração de código

O [*forking workflow*][forking-workflow] é uma estratégia de **fluxo de integração de código** que prevê que cada desenvolvedor trabalhe em sua própria cópia do repositório do projeto, adicionando suas modificações em um branch relacionado com a demanda atual e enviando sua entrega através de uma proposta de evolução do projeto, onde a equipe pode revisá-la, discutí-la e então integrá-la ao repositório original quando todos os requisitos estiverm cumpridos.

É assim que fazemos aqui na Nelogica, portanto vamos entender como funciona cada etapa desse processo praticando com o envio das suas respostas!
<img src="/ico/blob_cheer.png" width="30"/>

[forking-workflow]: https://www.atlassian.com/br/git/tutorials/comparing-workflows/forking-workflow


## 1. Fork
#### Começamos realizando um [fork][fork] do repositório que desejamos evoluir.

Vá até o Gitlab, na página do projeto [Welcome][repo]. Clique no botão de **fork** à direita do título do repositório e selecione como namespace destino o seu usuário.

![Fork](/img/gitflow-1.png)

Isso copiará todo o projeto para um novo repositório, sob o qual você terá total autonomia.

[fork]: /1.intro/gitlab.md#fork
[repo]: https://gitlab.nelogica.com.br/welcome/welcome


## 2. Clone
#### Fazemos, então, um clone do repositório recém criado

Para isso, abra seu terminal e navegue até a pasta onde deseja clonar seu projeto. Para os exemplos usaremos o diretório `C:/Fontes`.

```
PS C:/Fontes> git clone git@gitlab.nelogica.com.br:<seu-usuario>/welcome.git
```

Isso irá copiar o código do repositório *remoto* para nosso computador *local*, onde poderemos trabalhar nas nossas modificações.


## 3. Remote
#### Referenciamos o repositório remoto oficial
```
PS C:/Fontes/welcome> git remote add upstream git@gitlab.nelogica.com.br:welcome/welcome.git
```

Essa etapa serve para que possamos acompanhar as modificações de outros desenvolvedores que venham a ser integradas no repositório original antes de finalizarmos nosso trabalho e assim possamos manter nosso branch atualizado.


## 4. Branch
#### Criamos um novo branch, nomeando-o de acordo com a evolução que realizaremos
```
PS C:/Fontes/welcome> git branch --move answers
```

Com isso temos um ambiente isolado para finalmente aplicar nossas modificações de maneira organizada.


## 5. Add
#### Alteramos os arquivos do projeto para que se comportem conforme o objetivo, nos certificando que está tudo como gostariamos

Conforme esteja satisfeito com suas respostas, as adicione em seu changelog com o seguinte comando:
```
PS C:/Fontes/welcome> git add --all
```

## 6. Commit
#### Selamos nossas modificações em um commit

Ao concluir um determinado conjunto de evoluções, os *embrulhamos* através de uma mensagem de referência, incrementando a história do nosso branch.
```
PS C:/Fontes/welcome> git commit --message "minhas respostas do welcome"
```

## 7. Push
#### Fazemos um push para nosso repositório remoto, atualizando-o com as alterações que fizemos localmente
```
PS C:/Fontes/welcome> git push origin answers
```

Com isso sincronizamos nossas modificações novamente com o Gitlab, replicando o que temos localmente em nosso repositório remoto.


## 8. Merge-request
#### Abrimos um merge-request, sinalizando nossa proposta de evolução ao código base, de onde o fluxo começou

No Gitlab, na página do seu repositório, clique em **Merge Requests** no menu lateral e então em **Novo merge request**. Você chegará a uma página onde poderá selecionar os branchs (origem e destino) envolvidos nessa ação.
Após selecioná-los, clique em **Comparar branchs e continuar**.

![Merge Request](/img/gitflow-2.png)

Para concluir, insira um *título* coerente com a proposta de evolução sugerida, adicione uma *descrição* relatando mas minucias das suas modificações, se atrubua como *responsável* e selecione seu facilitador como revisor.

> Vale pontuar que, caso você realize novas modificações no seu branch remoto, estas serão incluídas imediatamente ao seu merge-request. Portanto, caso tenha esquecido de algo ou queira fazer mais alguma inclusão, fique a vontade!


## 9. Integração
Para finalizar, os mantenedores do projeto podem revisar as modificações sugeridas, o time tem um espaço para discutí-las, automatizações são disparadas, mais modificações podem ser solicitadas, para então, o merge-request ser aprovado e suas evoluções, por fim, serem integradas ao código do projeto original.


---
## Parabéns! Suas contribuições fazem parte do projeto
#### Agora que você conhece todo o processo, vamos praticar?
Configure sua cópia do repositório localmente e siga sua leitura. Ao final de cada módulo, vá a pasta *answers* e preencha suas respostas. Quando tiver tudo finalizado, prossiga o fluxo de integração de código até a abertura do seu merge-request. \
Seu facilitador, então, revisará suas respostas e lhe ajudará a compreender melhor o que for preciso.

> Esse material permanecerá disponível mesmo depois que você concluir o Welcome. <br> Assim, sempre que quiser, retorne e revise o que precisar.
<img src="/ico/meow_nerd2.png" width="30"/>
