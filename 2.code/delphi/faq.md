
## Problemas comuns

Para a correta execução dos scripts propostos neste tutorial via PowerShell, pode vir a ser necessário a execução do seguinte código em um PowerShell com permissões de administrador.

```bash
Set-ExecutionPolicy -ExecutionPolicy Unrestricted
```

### FastMM4 não funciona

- A DLL pré-compilada não foi adicionada em `%programfiles(x86)%\Embarcadero\Studio\18.0\bin` (`18.0` a versão do Delphi instalado)
- O caminho para a pasta `FastMM4` não foi adicionado ao library path do Delphi: `Tools > Options > Environment Options > Delphi Options > Library` ou, para o Rio, `Tools > Options > Language > ...`
- Foi usado a DLL/o path para 64bits mas o projeto é 32bits
- Foi usado a DLL/o path para 32bits mas o projeto é 64bits

### IDE está quebrando o encoding dos arquivos

- Atualizar o Delphi do 10.3 para o 10.3.1

## Erros comuns

###    CREATE de TIMER no EXECUTE e não no CREATE

Criando um timer no construtor da classe não no executeCreate (método executado na inicialização da thread da classe Daemon), faz o timer ser disparado antes da thread ser iniciada e fora do contexto dela, podendo disparar acesso a variáveis não inicializadas.

Errado:

```pascal
var m_AliveTimer : TTimer;
...
procedure TConfigurationDaemon.Create;
var
  strFile : String;
begin
	m_AliveTimer          := TTimer.Create(nil);
end

procedure TConfigurationDaemon.ExecuteCreate;
var
  strFile : String;
begin
	writeLn('do nothing');
end
```

Certo:


```pascal
var m_AliveTimer : TTimer;
...
procedure TConfigurationDaemon.ExecuteCreate;
var
  strFile : String;
begin
	m_AliveTimer          := TTimer.Create(nil);
end
```



###    Try Except no loop de processamento de mensagens:

O Try Except deve ser inserido fora do loop de processamento. Se for encontrado um erro durante este deve ser tratado e deve ser quebrada a ordem de execução do loop.

Errado:

```pascal
While condition do
	begin
		try
			ProcessMessage(pAuxArray, nMsgID, nCount);
		end;
		except
			on E : Exception do
                ////////////////////////////////////////////////////////////////////////////
                // Handle Exception
	end;
end;
```

Certo:
```pascal
try
	While condition do
    begin
		ProcessMessage(pAuxArray, nMsgID, nCount);
    end;
end;
except
	on E : Exception do
    ////////////////////////////////////////////////////////////////////////////
    // Handle Exception
end;
```



###    Loop WHILE e REPEAT com '>= 'ao invés de apenas '=' :

Para permitir melhor analise de código deve-se definir sempre os limites exatos dos loops de repetição ([Rule 2](https://drive.google.com/file/d/1b6bldpv6A97YqENa_ikLaC1PEk5-suLg/view)). Isso também faz com que o código seja mais tolerante a comportamentos inesperados.

Errado
```pascal
K : Integer;
begin
	K := 7;
	repeat
		writeln(IntToStr(K));
		Inc(K);
	until (K = 10);
end;
```
Correto
```pascal
K : Integer;
begin
	K := 7;
	repeat
		writeln(IntToStr(K));
		Inc(K);
	until (K >= 10);
end;
```


###    Não enviar ImAlive durante o loop de processamento de mensagem:

ImAlive deve ser enviado somente  durante o processamento do buffer, ele não pode ser executado caso esteja sendo realizado o processamento de um pacote de mensagens. Então é preciso fazer uma verificação para garantir. [source](https://gitlab.nelogica.com.br/qa/exchangecomparerservice/blob/develop/source/ExchangeTranslateAndDispatchU.pas)

Errado :

```pascal
nStart := 0;
Repeat
  if (nSize - nStart >= 3) then
    begin
      ////////////////////////////////////////////////////////////////////////
      // Verifica se tem pelo menos um pacote completo
      nCount := VerifyBuffer(@pBuffer[nStart], nSize - nStart, nMsgID);
      if ((nCount > 0) and (nMsgID <> c_nMsgInvalid)) then
        begin
          ////////////////////////////////////////////////////////////////////
          // Processa o pacote
          try
            ProcessMessage(@pBuffer[nStart], nMsgID, nCount);
          except
            on E : Exception do
              LogError(m_EventWriter, m_Instrumentation, 'TExchangeTranslateAndDispatch.ProcessData : Exception ('+IntToStr(Integer(nMsgID))+'): ' + E.ClassName + ' : ' + E.Message);
          end;
        end
      else if (nMsgID = c_nMsgInvalid) then
        m_EventWriter.WriteMsg('TExchangeTranslateAndDispatch.ProcessData: Mensagem inválida');
      ////////////////////////////////////////////////////////////////////////
      // Reposiciona o início do buffer para o próximo pacote
      nStart := nStart + nCount;
    end
  else
    nCount := 0;
    m_EventWriter.WriteMsg('TExchangeTranslateAndDispatch.ProcessData: sending alive');
    SendAlive;
Until (nCount <= 0) or (nSize = nStart);
```

Certo :


```pascal
nStart := 0;
Repeat
  if (nSize - nStart >= 3) then
    begin
      ////////////////////////////////////////////////////////////////////////
      // Verifica se tem pelo menos um pacote completo
      nCount := VerifyBuffer(@pBuffer[nStart], nSize - nStart, nMsgID);
      if ((nCount > 0) and (nMsgID <> c_nMsgInvalid)) then
        begin
          ////////////////////////////////////////////////////////////////////
          // Processa o pacote
          try
            ProcessMessage(@pBuffer[nStart], nMsgID, nCount);
          except
            on E : Exception do
              LogError(m_EventWriter, m_Instrumentation, 'TExchangeTranslateAndDispatch.ProcessData : Exception ('+IntToStr(Integer(nMsgID))+'): ' + E.ClassName + ' : ' + E.Message);
          end;
        end
      else if (nMsgID = c_nMsgInvalid) then
        m_EventWriter.WriteMsg('TExchangeTranslateAndDispatch.ProcessData: Mensagem inválida');
      ////////////////////////////////////////////////////////////////////////
      // Reposiciona o início do buffer para o próximo pacote
      nStart := nStart + nCount;
    end
  else
    nCount := 0;
  //////////////////////////////////////////////////////////////////////////////
  ///  Garante que alive seja enviado enquanto estiver processando buffer
  if (MilliSecondsBetween(m_dtLastAlive, Now) > c_nAliveInterval)  then
    begin
      m_EventWriter.WriteMsg('TExchangeTranslateAndDispatch.ProcessData: sending alive');
      SendAlive;
    end;
Until (nCount <= 0) or (nSize = nStart);
```



###    TIMER só dispara se a thread não estiver ocupada:

É necessário desativar um timer se uma thread esta em execução. reativando novamente somente quando ela terminar sua operação.

Errado:

```pascal
procedure TConfigurationDaemon.OnTimerProcDB(Sender : TObject);
begin
	try
		if (not Terminated) then
		  begin
			if (SecondsBetween(Now, m_dtLastVerifyUpdateLogRequest) >= m_nVerifyUpdateLogRequestsIntervalSecs) then
			  begin
				VerifyUpdateLogRequests;
				m_dtLastVerifyUpdateLogRequest := Now;
			  end;
			 VerifyPendingCommands;
		  end;
  except
    on E : Exception do
		  //////////////////////////////////////////////////////////////////////////
		  // Handle Exception
  end;
end;
```

Certo:

```pascal
procedure TConfigurationDaemon.OnTimerProcDB(Sender : TObject);
begin
  m_AliveTimerDB.Enabled := False;
  try
    if (not Terminated) then
      begin
        if (SecondsBetween(Now, m_dtLastVerifyUpdateLogRequest) >= m_nVerifyUpdateLogRequestsIntervalSecs) then
          begin
            VerifyUpdateLogRequests;
            m_dtLastVerifyUpdateLogRequest := Now;
          end;
         VerifyPendingCommands;
      end;
  except
    on E : Exception do
		  //////////////////////////////////////////////////////////////////////////
		  // Handle Exception
  end;
  m_AliveTimerDB.Enabled := True;
end;
```

###    Não inicializar com nil e testar para nil:

Por default ponteiros são inicializados como nil. Então as duas condições abaixo serão verdadeiras.

```pascal
var
   P1,P2: ^Integer;     // P points to an Integer
begin
  P1 := nil;
  if P1 = nil then
    writeln('Something' );
  if P2 = nil then
    writeln('Another thing');
  readln;
end.
```



###    Dispose se atribuir nil e depois testar para nil:

Dispose ira destruir a variável, tornando impossível de testar para qualquer valor:

```pascal
var
  X: Integer;  // X and Y are Integer variables
  P: ^Integer;     // P points to an Integer
begin
  X := 17;      // assign a value to X
  P := @X;      // assign the address of X to P
  if P <> nil then
    writeln('Current Value: ', P^ );
  Dispose(P);
  if P <> nil then
    writeln('Doesnt run');
end.
```



###    Lista não tipada com tipos diferentes:

Delphi não permite utilização de listas com diferentes tipos internos, para isso é necessário criar um Record com partes variáveis. ([source](http://docwiki.embarcadero.com/RADStudio/Tokyo/en/Structured_Types_(Delphi)#Array_Types_and_Assignments))

```pascal
type
   // Declare a fruit record using case to choose the
   // diameter of a round fruit, or length and height ohterwise.
   TFruit = Record
     name : String;
     Case isRound : Boolean of // Choose how to map the next section
       True  :
         (diameter : Integer);  // Maps to same storage as length
       False :
         (length   : Integer;   // Maps to same storage as diameter
          width    : Integer);
   end;
var
   apple, banana : TFruit;
   List: TList<TFruit>;
begin
   // Set up the apple as round, with appropriate dimensions
   apple.name     := 'Apple';
   apple.isRound  := True;
   apple.diameter := 3;

   // Set up the banana as long, with appropriate dimensions
   banana.name    := 'Banana';
   banana.isRound := False;
   banana.length  := 7;
   banana.width   := 1;

   List := TList<TFruit>.Create;
   List.Add(apple);
   List.Add(banana);
   writeln('Fruit name : ',List.Items[0].name,', Diameter :',
    List.Items[0].diameter );
   writeln('Fruit name : ',List.Items[1].name,', Lenght : ',
    List.Items[1].length,', Width: ',List.Items[1].width );
end.
```



###    CAST não é conversão:

Casting é um modo de "forçar" uma variável em outra, podendo resultar em erro se for gerado uma entrada no formato errado. Deve-se sempre quando possível fornecer as variáveis no formato adequadas. Se não for possível, primeiramente é necessário realizar uma verificação do formato da variável que será aplicado o CAST.

Errado:
```pascal
var
	MyDog: TDog;
begin
	MyDog := TDog 	(MyAnimal);
	Text := MyDog.Eat;
end.
```

Acima não foi verificado se realmente o MyAnimal é do tipo TDog, e se MyAnimal for um TCat? Isso causara erros.
Logo é necessário uma verificação primeiro :

Certo:
```pascal
var
	MyDog: TDog;
begin
	if MyAnimal is TDog then
	begin
		MyDog := TDog (MyAnimal);
		Text := MyDog.Eat;
	end;
end.
```



###    Mensagem é primeiro enviada para fila de thread e depois para a janela de processo :

Antes de enviar o envio de uma mensagem ela é adicionada na fila da *thread*.

```pascal
procedure TPerformanceReportDaemon.Execute;
var
	MsgRec : TMsg;
begin
	//////////////////////////////////////////////////////////////////////////////
 	// Cria a fila da Thread
	PeekMessage(MsgRec, 0, WM_USER, WM_USER, PM_NOREMOVE);

	//////////////////////////////////////////////////////////////////////////////
  	// Processamento padrão
  while GetMessage(MsgRec, 0, 0, 0) do
   	begin
     	TranslateMessage(MsgRec);
		MessageHandler(MsgRec);
		DispatchMessage(MsgRec);
    end;
end;
```



###    Sessão FIX desconecta quando hora  e/ou local errada :

```pascal
begin
    ...
    try
      nDay       := StrToInt(Copy(strLine,  1, 2));
      nMonth     := StrToInt(Copy(strLine,  4, 2));
      nYear      := StrToInt(Copy(strLine,  7, 4));
      nHour      := StrToInt(Copy(strLine, 12, 2));
      nMin       := StrToInt(Copy(strLine, 15, 2));
      nSec       := StrToInt(Copy(strLine, 18, 2));
      Session.dtDisconnected := EncodeDateTime(nYear, nMonth, nDay, nHour, nMin, nSec, 0);
    except
      Session.dtDisconnected := 0;
	...
end;
```



###    Chamada de SocketClient.send() em thread diferente daquela onde está o SocketClient e o SocketServer:

Se for criado clientes em threads separadas de onde esta o socket server, pode gerar casos de concorrência para acesso de algum recurso, pois pode ser que seja acessado um recurso por duas threads diferentes. Então a forma mais adequada para evitar este conflito, é utilizar o  SocketClient.send()  de forma sequencia.

Certo:

```pascal
procedure THadesDaemon.SendStateToClients;
var
  arBuffer : array[0 .. 99] of Byte;
  nIndex   : Integer;
  nSize    : Integer;
  Client   : TWSocketClient;
begin
  if (m_SocketServer.ClientCount > 0) then
    begin
      m_EventWriter.WriteMsg('THadesDaemon.SendStateToClients : '+IntToStr(Integer(m_ConnectionState)));

      //////////////////////////////////////////////////////////////////////////
      // Constrói o pacote de estado
      nSize := BuildStatePacket(m_ConnectionState, m_Broker.AresState, m_Broker.AresVersion, @arBuffer[0], Length(arBuffer));

      //////////////////////////////////////////////////////////////////////////
      // Envia a mensagem
      if (nSize > 0) then
        begin
          //////////////////////////////////////////////////////////////////////
          // Percorre todos os clientes
          For nIndex := 0 to m_SocketServer.ClientCount-1 do
            begin
              Client := m_SocketServer.Client[nIndex];
              if (Client.State = wsConnected) then
                Client.Send(@arBuffer[0], nSize);
            end;
        end;
    end;
end;
```
