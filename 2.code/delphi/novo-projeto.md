## Criação de projeto

Abra o Delphi e crie um novo projeto, selecionando linha de comando.  
Clique com o botão direito no projeto, salvar como, e selecione o diretório `C:\fontes\welcome\new_project\new_project.dproj`

Crie um projeto Delphi que recebe como entrada um inteiro (por parâmetro de linha de comando) e retorna 1 se ele é um primo, zero caso contrário.

O binário (.exe) deve ser gerado no diretório `C:\fontes\welcome\new_project\bin` com o nome new_project.exe. Para isso, altere as propriedades do projeto (botão direto em cima do projeto, a direita, options, delphi compiler options, output directory).

Os intermediários (.dcu) também devem ser gerados no diretório `C:\fontes\welcome\new_project\dcu`  (botão direto em cima do projeto, a direita, options, delphi compiler options, package output directory).

Para validar que está tudo certo, compile o projeto no delphi e execute o script através da linha de comando (cmd ou powershell):

```bat
powershell -F scripts/validate_new_project.ps1
```
