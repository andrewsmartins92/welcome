# Tratamento de Demandas das Corretoras
Ao lidar com várias demandas de diferentes corretoras é muito importante que sejam priorizadas e executadas de maneira eficiente e que sempre respeitem os prazos e requisitos demandados pela as corretoras. Aqui segue uma breve documentação sobre a execução dessas demandas.

[Processo de Execução de Demandas de Corretoras](https://nelogica.sharepoint.com/:w:/s/Hades/EbIUbWxaLEJKqiEx_3QJe_cBO0O7WPGG9HixiyLhO5KoPA?e=1dYzml)