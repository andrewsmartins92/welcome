# Integração Contínua, Entrega e Deploy contínuos

Integração contínua consiste de usar as ferramentas supra citadas automaticamente para validar a integração do código fonte no release. Após a validação, pode-se gerar automaticamente as versões (entrega contínua) e inclusive colocá-los nos ambientes de produção automaticamente (Deploy Contínuo)

Algumas ferramentas são usadas para isso:

Gitlab (utilizada na nelogica): <https://about.gitlab.com/product/continuous-integration/>

Jenkins: <https://jenkins.io/>

TeamCity: <https://www.jetbrains.com/teamcity/>

## Questões

- Qual a diferença de Entrega Contínua e Deploy Contínuo?
- Pode existir Entrega Contínua sem automação? Porque?
- Como medir o ganho de produtividade por uma ferramenta de integração contínua?
- Qual a relação do *git branching model* e do processo de integração contínua?
