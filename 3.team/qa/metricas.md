# Métricas de código

*"O que não é medido, não é gerenciado"*
Norton Kaplan

Se não temos medidas sobre o código, não conseguimos gerenciar o mesmo. O objetivo de métricas de código é quantificar a qualidade, complexidade e etc do código.

## Complexidade Ciclomática

Número de ciclos no código, usado para medir quantos testes são necessários para cobrir

<https://en.wikipedia.org/wiki/Cyclomatic_complexity>

## Vocabulário

São métricas de software introduzidas por Maurice Howard Halstead em 1977 como parte de seu tratado sobre o estabelecimento de uma ciência empírica do desenvolvimento de software.

Uma conjunto de métricas que mede isso são as métricas de Halstead: https://en.wikipedia.org/wiki/Halstead_complexity_measures

## Questões

- Para o que serve a contagem de linhas de código?
- O que demonstra a proporção linhas de código/linhas de comentário?
- Como avaliar se as variáveis de um arquivo possuem nomes significativos?


# Cobertura de Código (Code Coverage)

Mede quanto do código foi exercitado em uma determinada execução. Existem vários tipos de cobertura, basicamente cada unidade do software pode ser usada: arquivo, função, linha, decisão (e.g. *if*).

Leituras recomendadas:
[http://www.matera.com/blog/post/cobertura-uma-metrica-para-aferir-testes](http://www.matera.com/blog/post/cobertura-uma-metrica-para-aferir-testes)
<https://en.wikipedia.org/wiki/Code_coverage>

## Questões

- Quais os tipos de cobertura que existem?
- Qual a correlação entre cobertura de código e a qualidade dos testes?
- Qual a cobertura esperada em um projeto de software maduro?


# Analisadores estáticos

Analisadores estáticos são ferramentas que extraem métricas de código e anti-padrões de projeto do código fonte. Existem analisadores para várias linguagens (pascal analyzer para Delphi, pep8 e pylint para python, cppcheck e clang-static-analyser para C++, pmd para java).

## Questões

- Quais os problemas que o Pascal Analyser Lite mostra para o projeto de exemplo desse repositório ?
