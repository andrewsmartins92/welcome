# Shell

Em computação, um *shell* é um programa usado para acessar serviços do sistema operacional, tanto por [linhas de commando][cli] como por uma [interface gráfica][gui].

Nesta dinâmica utilizaremos um terminal para executar alguns comandos, como o `git`. Essa é uma prática comum no dia-a-dia de um desenvolvedor e com o tempo você ficará craque nisso.

No caso do Windows, usaremos o PowerShell, que já vem instalado por padrão. Ao abrí-lo você identificará o diretório em que está, precedido de `PS`, conforme o exemplo a seguir:

```
PS C:\Windows> comando
output
PS C:\Windows> outro-comando                # sem output
PS C:\Windows>
```

[cli]: /4.extra/cli.md
[gui]: https://pt.wikipedia.org/wiki/Interface_gr%C3%A1fica_do_utilizador


### Alguns commandos básicos
Para interagir com um shell através de um terminal utilizamos a [interface de linhas de comando][cli] de um programa. O próprio sistema operacional também disponibiza algumas das principais operações que você já sabe fazer através da interface gráfica. A seguir listamos as mais básicas:

| Comando | Funcionamento |
| --- | --- |
| [`cd`][cd] | Navega entre diretórios |
| [`ls`][ls] | Lista arquivos no diretório atual |
| [`mkdir`][mkdir] | Cria um novo diretório |
| [`rm`][rm] | Remove um arquivo ou diretório |


[cd]: https://pt.wikipedia.org/wiki/Cd_(comando)
[ls]: https://pt.wikipedia.org/wiki/Ls
[mkdir]: https://pt.wikipedia.org/wiki/Mkdir
[rm]: https://pt.wikipedia.org/wiki/Rm_(Unix)
