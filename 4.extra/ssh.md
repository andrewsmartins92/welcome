# SSH

> <img src="/ico/blobwizard.png" width="30"/>
Acrônimo para [*Secure Shell*][ssh]

[ssh]: https://pt.wikipedia.org/wiki/Secure_Shell

SSH é um protocolo para transmissão segura de dados através da rede. Dentre suas utilidades está a de identificar o remetente da mensagem, o que é feito a partir de um par de chaves pública-privada geradas manualmente, permitindo que usuários ou programas loguem sem ter que especificar uma senha.

Geraremos a seguir um par de chaves para seu computador para posteriormente configurar sua autenticação automática em alguns serviços, como por exemplo o Gitlab.


### Gerando suas chaves

Depois de entender o que é um [Shell][shell], abra seu terminal e execute o seguinte comando:
```
PS C:\Windows> ssh-keygen
```
Você será, então, [*promptado*][prompt] 3 vezes:
1. Definição do arquivo destino que conterá sua chave
2. Inserção de senha para protegê-lo
3. Confirmação de senha

Porém sugerimos que deixe os campos com os valores *default*, registrando sua chave no arquivo `id_rsa` e sem senha.

```bash
$ssh-keygen  
Generating public/private rsa key pair.  
Enter file in which to save the key (/u//.ssh/id_rsa):

Created directory '/u//.ssh'.  
Enter passphrase (empty for no passphrase):  
Enter same passphrase again:  
Your identification has been saved in /u//.ssh/id_rsa.  
Your public key has been saved in /u//.ssh/id_rsa.pub.  
The key fingerprint is:  
SHA256:0alMyeCqhXaa/dTI0L6+1dMDGgwa8uo/T1kNT4SOH4k caraujo@NELOGICA117  
The key's randomart image is
+---[RSA 2048]----+
|      .  ..      |  
|     . o.+ .     |  
|  . . o+*.+      |  
|   + =E=+B       |  
|  o B ..S.+      |  
| . O + =.+ o     |  
|  = . B + o o    |  
| .  .+ o   . .   |  
|  ...+*.         |  
+----[SHA256]-----+
```

Pronto, seu par de chaves SSH está criado!

[prompt]: https://www.dicio.com.br/prompt/
[shell]: /4.extra/shell.md
