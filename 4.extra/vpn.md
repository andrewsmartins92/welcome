# VPN

> <img src="/ico/blobwizard.png" width="30"/>
Acrônimo para *Virtual Private Network*, que em português significa
[rede privada virtual][vpn]

[vpn]: https://pt.wikipedia.org/wiki/Rede_privada_virtual

Por segurança, certos documentos, servidores e ferramentas só estão acessiveis através da **rede local da Nelogica**, sendo assim para acessá-los de fora da nossa sede será preciso estabelecer uma conexão segura. E é o que vamos fazer agora!

### FortClient

Primeiro instale o [FortClient VPN][fortclient]. Quando instalado, abra a plicação e clique em **Configurar a VPN**. Por fim, insira as seguintes informações e clique em salvar:

| Campo | Preenchimento |
| --- | --- |
| **VPN** | SSL-VPN |
| **Nome da Conexão** | OfficePOA |
| **Gateway Remoto** | 201.16.215.19:10443 <br> 189.114.92.131:10443 |
| **Certificado do cliente** | Nenhum |
| **Autenticação** | Salvar Login |
| **Usuário** | * |

\* Seu usuário e senha de acesso à VPN foram enviados para seu e-mail interno, "Acesso a Rede Nelogica"

![FortClient](/img/vpn-1.png)

Após configurar a VPN, vá para a aba **Acesso Remoto**, insira seu usuário e senha da rede interna e clique em **Conectar**. Tudo pronto!
<img src="/ico/blob-wink.gif" width="30"/>

[fortclient]: https://filestore.fortinet.com/forticlient/downloads/FortiClientVPNOnlineInstaller_6.4.exe
