## CLI
Acrónimo para *Command line interface*, que em português significa [interface de linha de comando][cli]

[cli]: https://pt.wikipedia.org/wiki/Interface_de_linha_de_comandos


## GUI
Acrónimo para *Graphical user interface*, que em português significa [interface gráfica de usuário][gui]

[gui]: https://pt.wikipedia.org/wiki/Interface_gr%C3%A1fica_do_utilizador


## SHA-1
Acrônimo para *Secure Hash Algorithm*, que em português significa
[algoritmo de dispersão seguro][sha1]

[sha1]: https://pt.wikipedia.org/wiki/SHA-1


## Shell
Programa usado para acessar serviços do sistema operacional


## SSH
Acrônimo para *Secure Shell*. Um protocolo para transmissão segura de dados através da rede


## URL
Acrônimo para *Uniform Resource Locator*. Refere-se ao endereço onde encontrar um determinado recurso na rede

## VCS
Acrônimo para *Version Control System*, em português [sistema de controle de versões][vcs]

[vcs]: https://pt.wikipedia.org/wiki/Sistema_de_controle_de_vers%C3%B5es


## VPN
Acrônimo para *Virtual Private Network*, em português [rede privada virtual][vpn]

[vpn]: https://pt.wikipedia.org/wiki/Rede_privada_virtual
