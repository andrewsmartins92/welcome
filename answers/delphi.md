## Questões

Obs: Em markdown, pode-se colocar snippets para exemplificar códigos-fonte. Exemplo:
```markdown
​```  delphi
	function a(int x)  
	begin  
		result := 1;  
	end  
​```
```

- Como se define uma classe em Delphi?
- Qual a diferença entre uma variável de módulo e uma variável global?
- O que são propriedades(properties)?
- Qual a diferença entre procedure e function?
- Qual o formato do cabeçalho usado pela Nelogica? Onde ele deve ser inserido?
- Como deve-se declarar um variável de módulo cujo tipo é um inteiro?
- Porque usar recursão em sistemas críticos não é recomendado?
- Qual o escopo indicado para uma variável?


# Codificação

## Questões

Responda as questões no mesmo formato das seções anteriores.

- Como adicionar um diretório ao path do delphi do projeto ?
- Como mudar o diretório de geração de binários ?
- O que são DCUs ?
- Qual a diferença de dproj e groupproj ?
- Quais as versões de Delphi disponíveis e quais suas diferenças ? (e.g. Rio, Tokyo, Seatle)
