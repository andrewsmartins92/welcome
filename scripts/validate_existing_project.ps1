$path_ok = Test-Path ./existing_project/mulU.pas -PathType Leaf 
if ($path_ok) {
	echo "mulU.pas correct!"
}
else {
	echo "Wrong mulU.pas path :("; 
	exit;
}
$path_ok = Test-Path ./existing_project/divU.pas -PathType Leaf 
if ($path_ok) {
	echo "Binary correct!"
}
else {
	echo "Wrong binary path :("; 
	exit;
}
$path_ok = Test-Path ./existing_project/bin/existing_project.exe -PathType Leaf 
if ($path_ok) {
	echo "Binary correct!"
}
else {
	echo "Wrong binary path :("; 
	exit;
}
$ret = & ./existing_project/bin/existing_project.exe sum 2 2
if ($ret -ne 4)
{
  echo "Wrong sum! 2 + 2 4"
  exit
 }
 
 $ret = ./existing_project/bin/existing_project.exe sub 5 2
if ($ret -ne 3)
{
  echo "Wrong subtraction! 5 - 2 = 3"
  exit
 }
 
 $ret = ./existing_project/bin/existing_project.exe mul 3 3
if ($ret -ne 9)
{
  echo "Wrong multiplication! 3 * 3 = 9"
  exit
 }
 
 $ret = ./existing_project/bin/existing_project.exe div 8 2
if ($ret -ne 4)
{
  echo "Wrong sub ! 8 / 2 = 4"
  exit
 }
 echo "Results correct!"
